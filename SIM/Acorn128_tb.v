module Acorn128_tb ();


parameter	inClkp = 10;
reg 		inClk = 1'b0;

always
begin
	#(inClkp/2) inClk = !inClk;
end

reg 			inKeyWr 		= 1'b0;
reg			inIVWr  		= 1'b0;
reg			inADDataWr  = 1'b0;
reg			inPDataWr	= 1'b0;
reg			inData		= 1'b0;
reg [127:0]	inKeyData	= 128'b1;
reg [127:0] inIVData 	= 128'b0;
wire [127:0] outTagData;
wire 			 outData;
wire 			 outBusy;

Acorn128 acorn128(

					.inClk(inClk),
					.inExtKeyWr(inKeyWr),
					.inExtIVWr(inIVWr),
					.inADDataWr(inADDataWr),
					.inPDataWr(inPDataWr),
					.inData(inData),
					.inKeyData(inKeyData),
					.inIVData(inIVData),
					.outBusy(outBusy),
					.outData(outData),
					.outTagData(outTagData)
);

integer i;

always
begin
	#(inClkp);
	inKeyWr = 1'b1; inIVWr = 1'b1; inKeyData = 128'b0; #(inClkp);
	inKeyWr = 1'b0; inIVWr = 1'b0; inKeyData = 128'b0; #(inClkp);
	for(i = 0; i<1800; i=i+1)begin
		#(inClkp);
	end
	for(i = 0; i< 1500; i = i+1 )begin
		if(outBusy == 1'b0 )begin
			inData = 1'b1; inPDataWr = 1'b1; i = 1800;
		end
		#(inClkp);
	end
	inPDataWr = 1'b0;	#(inClkp);
	for(i = 0; i< 1500; i = i+1 )begin
		#(inClkp);
	end
	
	$stop;
	
end


endmodule
