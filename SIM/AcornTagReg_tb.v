module AcornTagRegTb ();

parameter 			inClkp = 10;
reg 					inClk = 1'b0;
reg					inData = 1'b0;
reg					inDataWr = 1'b0;

wire 	[127:0] 		outData;

always
begin
			#(inClkp/2) inClk = !inClk;
end
////////////////////////////////////////////

integer i;

AcornTagReg acornTagReg (
		.inClk(inClk),
		.inData(inData),
		.inDataWr(inDataWr),
		.outData(outData)
);

always
begin
	#(inClkp);
	for(i=0; i<128; i=i+1)begin
		inDataWr = 1'b1; inData = 1'b1; #(inClkp);  
	end
	inDataWr = 1'b0; #(inClkp);
	$stop;
	
end


endmodule
