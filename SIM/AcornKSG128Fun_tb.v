module AcornKSG128Fun_Tb ();

parameter 			inClkp = 10;

reg 					inClk = 1'b0;

reg 	[7:0]				inB12  = 8'h00;
reg	[7:0]				inB154 = 8'h00;
reg	[7:0]				inB235 = 8'h00;
reg	[7:0]				inB61  = 8'h00;
reg	[7:0]				inB193 = 8'h00;
reg	[7:0]				inB230 = 8'h00;
reg 	[7:0]				inB111 = 8'h00;
reg 	[7:0]				inB66  = 8'h00;


wire 	[7:0] 		outData;

/////////////////////////////////////////////

always
begin
			#(inClkp/2) inClk = !inClk;
end
////////////////////////////////////////////

integer i;


AcornKSG128Fun acornKSG128Fun (
	.inB12(inB12),
	.inB154(inB154),
	.inB235(inB235),
	.inB61(inB61),
	.inB193(inB193),
	.inB230(inB230),
	.inB111(inB111),
	.inB66(inB66),
	.outData(outData)
);


always
begin
		#(inClkp);  
		inB12 = 8'h11; #(inClkp);  
		inB12 = 8'h15; #(inClkp);  
	$stop;
end


endmodule
