module AcornStateReg_Tb ();

parameter 			inClkp = 10;

reg 					inClk = 1'b0;
reg               inIntKeyWr = 1'b0;
reg 					inIntIVWr  = 1'b0;
reg					inXor      = 1'b0;
reg					inDataWr = 1'b0;
reg	[7:0] 		inData = 1'b0;
reg					inADDataWr = 1'b0;
reg					inPDataWr  = 1'b0;
reg 					inKeyWr    = 1'b0;
reg					inIVWr 	  = 1'b0;
reg	[127:0]		inKeyData  = 128'b0;
reg	[127:0]		inIVData   = 128'b0;
reg   [292:0]     inAStateData 	  = 293'b0;



wire 	[292:0] 		outData;
wire 	[7:0]	   	outMi;

/////////////////////////////////////////////

reg  	[7:0]			inCAi = 8'hff;
reg	[7:0]			inCBi = 8'hff;

wire  [292:0]		outDataF;
wire 	[7:0] 		outKSi;
wire  [7:0]       outFBi;
wire  [7:0]       outS0;
wire  [7:0]       outB107;
wire  [7:0]       outB244;
wire  [7:0]       outB23;
wire  [7:0]       outB160;
wire  [7:0]       outB196;


always
begin
			#(inClkp/2) inClk = !inClk;
end
////////////////////////////////////////////

integer i;

AcornStateReg acornStateReg (
		.inClk(inClk),
		.inStateData(outDataF),
		.inData(inData),
		.inIntKeyWr(inIntKeyWr),
		.inIntIVWr(inIntIVWr),
		.inXor(inXor),
		.inPData(inPData),
		.inADDataWr(inADDataWr),
		.inPDataWr(inPDataWr),
		.inExtKeyWr(inKeyWr),
		.inExtIVWr(inIVWr),
		.inKeyData(inKeyData),
		.inIVData(inIVData),
		.inStateDataWr(inDataWr),
		.outMi(outMi),
		.outData(outData)
);

AcornStateUpdateFun acornStateUpdateFun(
		.inData(outData),
		.inCAi(inCAi),
		.inCBi(inCBi),
		.inMi(outMi),
		.outData(outDataF),
		.outKSi(outKSi),
		.outFBi(outFBi),
		.outS0(outS0),
		.outB107(outB107),
		.outB244(outB244),
		.outB23(outB23),
		.outB160(outB160),
		.outB196(outB196)
);

always
begin
	inPDataWr = 1'b1; inData = 8'hf0; #(inClkp);  
	for(i=0; i<224; i=i+1)begin
		inDataWr = 1'b1;  #(inClkp);  
	end
	#(inClkp);
	$stop;
end


endmodule
