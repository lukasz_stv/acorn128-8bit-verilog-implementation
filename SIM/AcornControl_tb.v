module AcornControl_tb ();

parameter	inClkp = 10;
reg 	   	inClk = 1'b0;

always
begin
	#(inClkp/2) inClk = !inClk;
end

reg 			 inKeyWr 		= 1'b0;
reg			 inIVWr  		= 1'b0;
reg			 inADDataWr  = 1'b0;
reg			 inPDataWr	= 1'b0;
wire 			 outStateDataWr;
wire 			 outOutDataWr;
wire 			 outOutTagWr;
wire 			 outBusy;
wire [7:0]	 outCAi;
wire [7:0]	 outCBi;
wire 			 outIntKeyWr;
wire 			 outIntIVWr;
wire 			 outXor;
wire 			 outDataEnd;

AcornControl acornControl(
					.inClk(inClk),
					.inKeyWr(inKeyWr),
					.inIVWr(inIVWr),
					.inADDataWr(inADDataWr),
					.inPDataWr(inPDataWr),
					.outStateDataWr(outStateDataWr),
					.outOutDataWr(outOutDataWr),
					.outOutTagWr(outOutTagWr),
					.outBusy(outBusy),
					.outCAi(outCAi),
					.outCBi(outCBi),
					.outIntKeyWr(outIntKeyWr),
					.outIntIVWr(outIntIVWr),
					.outDataEnd(outDataEnd),
					.outXor(outXor)
);



reg	[7:0]			inData  = 8'b0;
reg	[127:0]		inKeyData  = 128'b0;
reg	[127:0]		inIVData   = 128'b0;
reg   [292:0]     inStateData =  293'b0;



wire 	[292:0] 		outData;
wire 	[7:0]			outMi;

/////////////////////////////////////////////

reg  	[7:0]			inCAi = 8'b0;
reg	[7:0]			inCBi = 8'b0;

wire [292:0]		outDataF;
wire [7:0]			outKSi;


////////////////////////////////////////////

wire [127:0] 	   TagRegOutData;

///////////////////////////////////////////

wire 	[7:0]			OutOutData;




AcornStateReg acornStateReg (
		.inClk(inClk),
		.inStateData(outDataF),
		.inData(inData),
		.inIntKeyWr(outIntKeyWr),
		.inIntIVWr(outIntIVWr),
		.inXor(outXor),
		.inDataEnd(outDataEnd),
		.inADDataWr(inADDataWr),
		.inPDataWr(inPDataWr),
		.inExtKeyWr(inKeyWr),
		.inExtIVWr(inIVWr),
		.inKeyData(inKeyData),
		.inIVData(inIVData),
		.inStateDataWr(outStateDataWr),
		.outMi(outMi),
		.outData(outData)
);

AcornStateUpdateFun acornStateUpdateFun(
		.inData(outData),
		.inCAi(outCAi),
		.inCBi(outCBi),
		.inMi(outMi),
		.outData(outDataF),
		.outKSi(outKSi)

);


AcornTagReg acornTagReg(
		.inClk(inClk),
		.inData(outKSi),
		.inDataWr(outOutTagWr),
		.outData(TagRegOutData)
);

AcornOutDataReg acornOutDataReg(
		.inClk(inClk),
		.inData(outMi),
		.inDataWr(outOutDataWr),
		.inKSi(outKSi),
		.outData(OutOutData)


);



integer i;
integer j;

always
begin
	#(inClkp);
	inKeyWr = 1'b1; inIVWr = 1'b1; inIVData = 128'h01010101010101010101010101010101; inKeyData = 128'h01010101010101010101010101010101; #(inClkp);
	inKeyWr = 1'b0; inIVWr = 1'b0; inIVData = 128'b0; inKeyData = 128'b0; #(inClkp);
	for(i = 0; i<224; i=i+1)begin
		if( outBusy == 1'b0 )begin
			inData = 8'h01; inADDataWr = 1'b1; i = 250;
		end
		#(inClkp);
	end
	for(i = 0; i<15; i = i+1)begin
		#(inClkp);
	end
	inADDataWr = 1'b0;	#(inClkp);
	
	
	for(i = 0; i< 40; i = i+1 )begin
		if(outBusy == 1'b0 )begin
			inData = 8'h01; inPDataWr = 1'b1; i = 50;
		end
		#(inClkp);
	end
	for(i = 0; i<15; i = i+1)begin
		#(inClkp);
	end
	inPDataWr = 1'b0;	#(inClkp);
	
	for(i = 0; i< 200; i = i+1 )begin
		#(inClkp);
	end
	$stop;
	
end



endmodule
