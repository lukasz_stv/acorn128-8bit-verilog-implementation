module AcornControl (
		input  wire  			inClk, 				
		input  wire 			inKeyWr, 			
		input  wire       	inADDataWr,		
		input  wire 			inPDataWr,		
		input  wire       	inIVWr,			
		output wire  [7:0]	outCAi,			
		output wire  [7:0]	outCBi,		
		output wire 			outDataEnd,
		output wire				outStateDataWr,
		output wire 			outIntKeyWr,     
 		output wire 			outIntIVWr,		
		output wire 			outOutDataWr, 
		output wire 			outOutTagWr,
		output wire 			outXor,	
		output wire 			outBusy			
		
);

reg [15:0] regCounter   = 16'b0;
reg 		  regDataWr    = 1'b0;


always @ (posedge(inClk)) begin
	if ( inKeyWr || inIVWr || regCounter != 1'b0 ) begin
		if ( regCounter == 16'd256 & inPDataWr == 1'b1 ) begin
			regCounter <= regCounter + 1'b1;
			regDataWr  <= 1'b1;
		end else begin
		if ( inADDataWr == 1'b0 & inPDataWr == 1'b0 & ((regCounter != 16'd385 & regDataWr == 1'b1 ) || (regCounter != 16'd384 & regDataWr == 1'b0)) )begin 
				regCounter <= regCounter + 1'b1;
		end
		end
		if (( regCounter == 16'd385 & regDataWr == 1'b1) || (regCounter == 16'd384 & regDataWr == 1'b0 )) begin
			regCounter <= 1'b0;
		end
	end
end

assign outBusy 	  		= (( regCounter != 1'b0 & regCounter != 16'd224 & regCounter != 16'd256 & regCounter != 16'd257 ) || ( regCounter == 16'd257 & inPDataWr == 1'b0 ) ) ? 1'b1 : 1'b0;
assign outStateDataWr	= ( regCounter != 1'b0 ) 	  ? 1'b1 : 1'b0; 
assign outIntIVWr 		= ( regCounter == 16'd16 )   ? 1'b1 : 1'b0;
assign outIntKeyWr 		= ( regCounter == 16'd32 || regCounter == 16'd48  || regCounter == 16'd64  || regCounter == 16'd80  || regCounter == 16'd96  || regCounter == 16'd112  || regCounter == 16'd128  || regCounter == 16'd144  || regCounter == 16'd160  || regCounter == 16'd176  || regCounter == 16'd192  || regCounter == 16'd208 ) ? 1'b1 : 1'b0; 
assign outXor           = ( regCounter == 16'd32 ) ? 1'b1 : 1'b0;
assign outDataEnd 		= ( regCounter == 16'd224 & inADDataWr == 1'b0 ) || (( regCounter == 16'd257 & inPDataWr  == 1'b0 & regDataWr == 1'b1 ) || (regCounter == 16'd256 & inPDataWr == 1'b0 )) ? 1'b1 : 1'b0;
assign outOutDataWr 		= ( regCounter == 16'd257 & regDataWr == 1'b1 )   ? 1'b1 : 1'b0;
assign outOutTagWr  		= ( regCounter >= 16'd368 ) ? 1'b1 : 1'b0;
assign outCAi				= ( regCounter <= 16'd240 || ( regCounter > 16'd256 & regCounter <= 16'd272 & regDataWr == 1'b0 ) || ( regCounter > 16'd256 & regCounter <= 16'd273 & regDataWr == 1'b1 ) || ( regCounter > 16'd289  & regDataWr == 1'b1  ) || 
 ( regCounter > 16'd288  & regDataWr == 1'b0 ) || ( inPDataWr == 1'b1 & regCounter == 16'd257 )) ? 8'hff : 8'h00;
assign outCBi				= ( regCounter <= 16'd256  || (regCounter > 16'd289 & regDataWr == 1'b1 ) || ( regCounter > 16'd288  & regDataWr == 1'b0 ))  ? 8'hff : 8'h00;


endmodule
