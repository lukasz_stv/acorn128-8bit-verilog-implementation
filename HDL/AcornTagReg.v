module AcornTagReg (
	input  wire 				inClk,
	input  wire [7:0]			inData,
	input  wire 				inDataWr,
	output wire [127:0] 		outData
);

reg 	[127:0] 	regData = 128'b0;

always @ (posedge(inClk)) begin
	if ( inDataWr == 1'b1 ) begin
		regData <=  (inData<<120) ^ (regData >> 8); 
	end
end

assign outData [ 127 : 120 ] = regData[ 7:0 ];
assign outData [ 119 : 112 ] = regData[ 15:8 ];
assign outData [ 111 : 104 ] = regData[ 23:16 ];
assign outData [ 103 : 96 ] = regData[ 31:24 ];
assign outData [ 95 : 88 ] = regData[ 39:32 ];
assign outData [ 87 : 80 ] = regData[ 47:40 ];
assign outData [ 79 : 72 ] = regData[ 55:48 ];
assign outData [ 71 : 64 ] = regData[ 63:56 ];
assign outData [ 63 : 56 ] = regData[ 71:64 ];
assign outData [ 55 : 48 ] = regData[ 79:72 ];
assign outData [ 47 : 40 ] = regData[ 87:80 ];
assign outData [ 39 : 32 ] = regData[ 95:88 ];
assign outData [ 31 : 24 ] = regData[ 103:96 ];
assign outData [ 23 : 16 ] = regData[ 111:104 ];
assign outData [ 15 : 8 ] = regData[ 119:112 ];
assign outData [ 7 : 0 ] = regData[ 127:120 ];

endmodule
