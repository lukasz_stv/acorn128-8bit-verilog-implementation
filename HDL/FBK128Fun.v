////////////////////////////
// Łukasz STEFAŃSKI       //
// 			  //
//        		  //
////////////////////////////


/* Blok FBK128Fun jest blokiem kombinacyjnym realizującym funkcje generowania bajtu sprzężenia zwrotnego 
na podstawie bajtów stanu, wejść sterujących ca_i i cb_i  oraz bajtu strumienia klucza ks_i 
Wyjściem modułu jest bajt f_i  */

module AcornFBK128Fun (
		input  wire [7:0]   inS0,
		input  wire [7:0]   inB107,
		input  wire [7:0]   inB244,
		input  wire [7:0]   inB23,
		input  wire [7:0]   inB160,
		input  wire [7:0]   inB196,
		input  wire [7:0]   inCAi,
		input  wire [7:0]   inCBi,
		input  wire [7:0]	  inKSi,
		output wire	[7:0]	  outData
);

assign outData = inS0 ^ (~(inB107)) ^ ((inB244 & inB23)^(inB244 & inB160)^(inB23 & inB160)) ^ ( inCAi & inB196 ) ^ ( inCBi & inKSi );

endmodule
