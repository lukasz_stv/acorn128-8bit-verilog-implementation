//Module Acorn128 is Top-Level entity of this project. 

module Acorn128 (
			input  wire 			inClk,       	
			input	 wire 			inExtKeyWr,     
			input  wire 			inExtIVWr,	
			input  wire 			inADDataWr,	
			input  wire 			inPDataWr,	
			input  wire [7:0]		inData,	
			input  wire [127:0]	inKeyData,	
			input  wire [127:0]	inIVData,	
			output wire 			outBusy,	
			output wire [7:0]		outData,	
			output wire [127:0]	outTagData
);

wire 				wireAcornControlOutCAi;
wire 				wireAcornControlOutCBi;
wire 				wireAcornControlOutStateDataWr;
wire 				wireAcornControlOutIntKeyWr;
wire 				wireAcornControlOutIntIVWr;
wire 				wireAcornControlOutXor;
wire 				wireAcornControlOutDataWr;
wire 				wireAcornControlOutTagWr;
wire 				wireAcornControlOutDataEnd;

wire [292:0] 	wireAcornStateRegOutData;
wire [7:0]		wireAcornStateRegOutMi;

wire [292:0] 	wireAcornStateUpdateFunOutData;
wire [7:0]		wireAcornStateUpdateFunOutKSi;

AcornControl acornControl (
			.inClk(inClk),
			.inKeyWr(inExtKeyWr),
			.inIVWr(inExtIVWr),
			.inADDataWr(inADDataWr),
			.inPDataWr(inPDataWr),
			.outCAi(wireAcornControlOutCAi),
			.outCBi(wireAcornControlOutCBi),
			.outStateDataWr(wireAcornControlOutStateDataWr),
			.outIntKeyWr(wireAcornControlOutIntKeyWr),
			.outIntIVWr(wireAcornControlOutIntIVWr),
			.outXor(wireAcornControlOutXor),
			.outOutDataWr(wireAcornControlOutDataWr),
			.outOutTagWr(wireAcornControlOutTagWr),
			.outDataEnd(wireAcornControlOutDataEnd),
			.outBusy(outBusy)
);

AcornStateReg acornStateReg (
			.inClk(inClk),
			.inStateDataWr(wireAcornControlOutStateDataWr),
			.inData(inData),
			.inStateData(wireAcornStateUpdateFunOutData),
			.inKeyData(inKeyData),
			.inIVData(inIVData),
			.inDataEnd(wireAcornControlOutDataEnd),
			.inADDataWr(inADDataWr),
			.inPDataWr(inPDataWr),
			.inIntKeyWr(wireAcornControlOutIntKeyWr),
			.inIntIVWr(wireAcornControlOutIntIVWr),
			.inExtIVWr(inExtIVWr),
			.inExtKeyWr(inExtKeyWr),
			.outData(wireAcornStateRegOutData),
			.outMi(wireAcornStateRegOutMi)
);

AcornStateUpdateFun acornStateUpdateFun (
			.inData(wireAcornStateRegOutData),
			.inCAi(wireAcornControlOutCAi),
			.inCBi(wireAcornControlOutCBi),
			.inMi(wireAcornStateRegOutMi),
			.outData(wireAcornStateUpdateFunOutData),
			.outKSi(wireAcornStateUpdateFunOutKSi)
);

AcornOutDataReg acornOutDataReg (
			.inClk(inClk),
			.inData(inPData),
			.inKSi(wireAcornStateUpdateFunOutKSi),
			.inDataWr(wireAcornControlOutDataWr),
			.outData(outData)
);

AcornTagReg acornTagReg (
			.inClk(inClk),
			.inData(wireAcornStateUpdateFunOutKSi),
			.inDataWr(wireAcornControlOutTagWr),
			.outData(outTagData)
);



endmodule
