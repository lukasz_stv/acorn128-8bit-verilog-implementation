////////////////////////////
// Łukasz STEFAŃSKI       //
// 			  //
//        		  //
////////////////////////////


/* AcornStateUpdateFun jest blokiem kombinacyjnym realizującym funkcje aktualizacji rejestru stanu */

module AcornStateUpdateFun (
	input  wire  [292:0]		inData, 
	input  wire  [7:0]      inCAi,
	input  wire  [7:0]		inCBi,
	input  wire  [7:0]		inMi,
	output wire  [292:0]    outData,
	output wire  [7:0]      outKSi,
	output wire  [7:0]      outFBi,
	output wire  [7:0]      outS0,
	output wire  [7:0]      outB107,
	output wire  [7:0]      outB244,
	output wire  [7:0]      outB23,
	output wire  [7:0]      outB160,
	output wire  [7:0]      outB196
);



//wire [292:0] AcornKSGFBK128FunInData;

/* Pomocnicze druty bajtów  */

wire  [7:0]  byte_12;
wire  [7:0]  byte_235;
wire  [7:0]  byte_244;
wire  [7:0]  byte_23;
wire  [7:0]  byte_160;
wire  [7:0]  byte_111;
wire  [7:0]  byte_66;
wire  [7:0]  byte_196;

wire  [7:0]  byte_230;
wire  [7:0] byte_193;
wire  [7:0]  byte_154;
wire  [7:0]  byte_107;
wire  [7:0]  byte_61;


/* Wejścia do bloku AcornKSG128Fun */

wire [7:0]   AcornKSG128FunInB12;
wire [7:0]   AcornKSG128FunInB154;
wire [7:0]   AcornKSG128FunInB235;
wire [7:0]   AcornKSG128FunInB61;
wire [7:0]   AcornKSG128FunInB193;
wire [7:0]   AcornKSG128FunInB230;
wire [7:0]   AcornKSG128FunInB111;
wire [7:0]   AcornKSG128FunInB66;

/* Wejścia do bloku AcornFBK128Fun  */

wire [7:0]   AcornFBK128FunInS0;
wire [7:0]   AcornFBK128FunInB107;
wire [7:0]   AcornFBK128FunInB244;
wire [7:0]   AcornFBK128FunInB23;
wire [7:0]   AcornFBK128FunInB160;
wire [7:0]   AcornFBK128FunInB196;

// Wyjścia funkcji KSG i FBK

wire [7:0]	 AcornKSG128FunOutData;
wire [7:0]	 AcornFBK128FunOutData;


AcornKSG128Fun acornKSG128Fun (
	.inB12(AcornKSG128FunInB12),
	.inB154(AcornKSG128FunInB154),
	.inB235(AcornKSG128FunInB235),
	.inB61(AcornKSG128FunInB61),
	.inB193(AcornKSG128FunInB193),
	.inB230(AcornKSG128FunInB230),
	.inB111(AcornKSG128FunInB111),
	.inB66(AcornKSG128FunInB66),
	.outData(AcornKSG128FunOutData)
);

AcornFBK128Fun acornFBK128Fun (
	.inS0(AcornFBK128FunInS0),
	.inB107(AcornFBK128FunInB107),
	.inB244(AcornFBK128FunInB244),
	.inB23(AcornFBK128FunInB23),
	.inB160(AcornFBK128FunInB160),
	.inB196(AcornFBK128FunInB196),
	.inCAi(inCAi),
	.inCBi(inCBi),
	.inKSi(AcornKSG128FunOutData),
	.outData(AcornFBK128FunOutData)
);

/* Przypisanie wartosci bajów pomocniczych */

assign   byte_12  = (inData[15 : 8]    >> 4 ) | (inData[23 : 16]   << 4 );
assign   byte_235 = (inData[239 : 232] >> 3 ) | (inData[247 : 240] << 5 );
assign   byte_244 = (inData[247 : 240] >> 4 ) | (inData[255 : 248] << 4 );
assign   byte_23  = (inData[23 : 16]   >> 7 ) | (inData[31 : 24]   << 1 );
assign   byte_160 = (inData[167 : 160]);
assign   byte_111 = (inData[111 : 104] >> 7 ) | (inData[119 : 112] << 1 );
assign   byte_66  = (inData[71 : 64]   >> 2 ) | (inData[79 : 72]   << 6 );
assign   byte_196 = (inData[199 : 192] >> 4 ) | (inData[207 : 200] << 4 );

assign   byte_230 = (inData[231 : 224] >> 6 ) | (inData[239 : 232] << 2 );
assign   byte_193 = (inData[199 : 192] >> 1 ) | (inData[207 : 200] << 7 );
assign   byte_154 = (inData[159 : 152] >> 2 ) | (inData[167 : 160] << 6 );
assign   byte_107 = (inData[111 : 104] >> 3 ) | (inData[119 : 112] << 5 );
assign   byte_61  = (inData[63 : 56]   >> 5 ) | (inData[71 : 64]   << 3 );

/* Przypisanie wejść do bloku AcornKSG128Fun */

assign 	AcornKSG128FunInB12  = byte_12;
assign 	AcornKSG128FunInB154 = byte_154 ^ byte_111 ^ byte_107;
assign 	AcornKSG128FunInB235 = byte_235;
assign   AcornKSG128FunInB61  = byte_61  ^ byte_23 ^ inData[7:0];
assign   AcornKSG128FunInB193 = byte_193 ^ byte_160 ^ byte_154;
assign   AcornKSG128FunInB230 = byte_230 ^ byte_196 ^ byte_193;
assign   AcornKSG128FunInB111 = byte_111;
assign   AcornKSG128FunInB66  = byte_66;


/* Przypisanie wejść do bloku AcornFBK128Fun */

assign   AcornFBK128FunInS0   = inData[7:0];
assign	 AcornFBK128FunInB107 = byte_107 ^ byte_66 ^ byte_61;
assign 	 AcornFBK128FunInB244 = byte_244;
assign   AcornFBK128FunInB23  = byte_23;
assign   AcornFBK128FunInB160 = byte_160;
assign   AcornFBK128FunInB196 = byte_196;



assign   outS0   = inData[7:0];
assign   outB107 = byte_107 ^ byte_66 ^ byte_61;
assign   outB244 = byte_244;
assign 	 outB23  = byte_23;
assign   outB160 = byte_160;
assign   outB196 = byte_196;


// Assign wyjscia


assign outData [7:0]  = inData[15:8];
assign outData [15:8] = inData[31:16];
assign outData [23:16] = inData[31:24];
assign outData [31:24] = inData[39:32];
assign outData [39:32] = inData[47:40];
assign outData [47:40] = inData[55:48];
assign outData [55:48] = inData[63:56] ^ ((byte_23 ^ inData[7 : 0] ) << 5);
assign outData [63:56] = inData[71:64] ^ ((byte_23 ^ inData[7 : 0] ) >> 3);
assign outData [71:64] = inData[79:72];
assign outData [79:72] = inData[87:80];
assign outData [87:80] = inData[95:88];
assign outData [95:88] = inData[103:96];
assign outData [103:96]  = inData[111:104] ^ ((byte_66 ^ byte_61) << 3);
assign outData [111:104] = inData[119:112] ^ ((byte_66 ^ byte_61) >> 5);
assign outData [119:112] = inData[127:120];
assign outData [127:120] = inData[135:128];
assign outData [135:128] = inData[143:136];
assign outData [143:136] = inData[151:144];
assign outData [151:144] = inData[159:152] ^ ((byte_111 ^ byte_107) << 2);
assign outData [159:152] = inData[167:160] ^ ((byte_111 ^ byte_107) >> 6);
assign outData [167:160] = inData[175:168];
assign outData [175:168] = inData[183:176];
assign outData [183:176] = inData[191:184];
assign outData [191:184] = inData[199:192] ^ ((byte_160 ^ byte_154) << 1);
assign outData [199:192] = inData[207:200] ^ ((byte_160 ^ byte_154) >> 7);
assign outData [207:200] = inData[215:208];
assign outData [215:208] = inData[223:216];
assign outData [223:216] = inData[231:224] ^ ((byte_196 ^ byte_193) << 6);
assign outData [231:224] = inData[239:232] ^ ((byte_196 ^ byte_193) >> 2);
assign outData [239:232] = inData[247:240];
assign outData [247:240] = inData[255:248];
assign outData [255:248] = inData[263:256];
assign outData [263:256] = inData[271:264];
assign outData [271:264] = inData[279:272];
assign outData [279:272] = inData[287:280];
assign outData [287:280] = inData[292:288] ^ ((byte_235 ^ byte_230) << 1) ^ ((AcornFBK128FunOutData ^ inMi) << 5) ; //
assign outData [292:288] = ((byte_235 ^ byte_230) >> 7) ^ ((AcornFBK128FunOutData ^ inMi  ) >> 3); //


assign outKSi  =  AcornKSG128FunOutData;
assign outFBi  =  AcornFBK128FunOutData;


endmodule
