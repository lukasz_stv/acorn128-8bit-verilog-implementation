module AcornStateReg (
	input	 wire				inClk,
	input  wire				inStateDataWr,
	input  wire [7:0]		inData,
	input  wire 			inADDataWr,
	input  wire 			inPDataWr,
	input  wire 			inIntKeyWr,
	input  wire 			inIntIVWr,
	input  wire 			inExtIVWr,
	input  wire 			inExtKeyWr,
	input	 wire 			inXor,
	input  wire 			inDataEnd,
	input  wire [127:0]  inKeyData,
	input  wire [127:0]  inIVData,
	input  wire [292:0]	inStateData,
	output wire [292:0]  outData,
	output wire [7:0] 	outMi

);

reg [292 :0] regData = 293'b0;
reg [127:0]  regInit = 128'b0;
reg [127:0]  regKey	= 128'b0;
reg [127:0]	 regIV   = 128'b0;

always @ (posedge(inClk)) begin
	if (inStateDataWr == 1'b1) begin
		regData <= inStateData;
		regInit <= regInit >> 8;
	end
	if (inExtIVWr == 1'b1 ) begin
		regData <= 293'b0;
		regIV[ 7 : 0 ]     <= inIVData[ 127 : 120 ];
		regIV[ 15 : 8 ]    <= inIVData[ 119 : 112 ];
		regIV[ 23 : 16 ]   <= inIVData[ 111 : 104 ];
		regIV[ 31 : 24 ]   <= inIVData[ 103 : 96 ];
		regIV[ 39 : 32 ]   <= inIVData[ 95 : 88 ];
		regIV[ 47 : 40 ]   <= inIVData[ 87 : 80 ];
		regIV[ 55 : 48 ]   <= inIVData[ 79 : 72 ];
		regIV[ 63 : 56 ]   <= inIVData[ 71 : 64 ];
		regIV[ 71 : 64 ]   <= inIVData[ 63 : 56 ];
		regIV[ 79 : 72 ]   <= inIVData[ 55 : 48 ];
		regIV[ 87 : 80 ]   <= inIVData[ 47 : 40 ];
		regIV[ 95 : 88 ] 	 <= inIVData[ 39 : 32 ];
		regIV[ 103 : 96 ]  <= inIVData[ 31 : 24 ];
		regIV[ 111 : 104 ] <= inIVData[ 23 : 16 ];
		regIV[ 119 : 112 ] <= inIVData[ 15 : 8 ];
		regIV[ 127 : 120 ] <= inIVData[ 7 : 0 ];
	end
	if (inExtKeyWr == 1'b1 )begin
		regData <= 293'b0; 	
		regKey[ 7 : 0 ]     <= inKeyData[ 127 : 120 ];
		regKey[ 15 : 8 ]    <= inKeyData[ 119 : 112 ];
		regKey[ 23 : 16 ]   <= inKeyData[ 111 : 104 ];
		regKey[ 31 : 24 ]   <= inKeyData[ 103 : 96 ];
		regKey[ 39 : 32 ]   <= inKeyData[ 95 : 88 ];
		regKey[ 47 : 40 ]   <= inKeyData[ 87 : 80 ];
		regKey[ 55 : 48 ]   <= inKeyData[ 79 : 72 ];
		regKey[ 63 : 56 ]   <= inKeyData[ 71 : 64 ];
		regKey[ 71 : 64 ]   <= inKeyData[ 63 : 56 ];
		regKey[ 79 : 72 ]   <= inKeyData[ 55 : 48 ];
		regKey[ 87 : 80 ]   <= inKeyData[ 47 : 40 ];
		regKey[ 95 : 88 ]   <= inKeyData[ 39 : 32 ];
		regKey[ 103 : 96 ]  <= inKeyData[ 31 : 24 ];
		regKey[ 111 : 104 ] <= inKeyData[ 23 : 16 ];
		regKey[ 119 : 112 ] <= inKeyData[ 15 : 8 ];
		regKey[ 127 : 120 ] <= inKeyData[ 7 : 0 ];
		
		regInit[ 7 : 0 ]     <= inKeyData[ 127 : 120 ];
		regInit[ 15 : 8 ]    <= inKeyData[ 119 : 112 ];
		regInit[ 23 : 16 ]   <= inKeyData[ 111 : 104 ];
		regInit[ 31 : 24 ]   <= inKeyData[ 103 : 96 ];
		regInit[ 39 : 32 ]   <= inKeyData[ 95 : 88 ];
		regInit[ 47 : 40 ]   <= inKeyData[ 87 : 80 ];
		regInit[ 55 : 48 ]   <= inKeyData[ 79 : 72 ];
		regInit[ 63 : 56 ]   <= inKeyData[ 71 : 64 ];
		regInit[ 71 : 64 ]   <= inKeyData[ 63 : 56 ];
		regInit[ 79 : 72 ]   <= inKeyData[ 55 : 48 ];
		regInit[ 87 : 80 ]   <= inKeyData[ 47 : 40 ];
		regInit[ 95 : 88 ]   <= inKeyData[ 39 : 32 ];
		regInit[ 103 : 96 ]  <= inKeyData[ 31 : 24 ];
		regInit[ 111 : 104 ] <= inKeyData[ 23 : 16 ];
		regInit[ 119 : 112 ] <= inKeyData[ 15 : 8 ];
		regInit[ 127 : 120 ] <= inKeyData[ 7 : 0 ];
		
		
	end
	if (inADDataWr == 1'b1 || inPDataWr == 1'b1 ) begin
		regInit <= inData | ( inData & regInit);
	end
	if (inDataEnd == 1'b1 )begin
		regInit <= 128'b1;
	end
	if ( inIntIVWr == 1'b1 ) begin
		regInit <= regIV;
	end
	
	if ( inIntKeyWr == 1'b1 ) begin
		if( inXor == 1'b1 )begin
			regInit <= 8'b1 ^ regKey;
		end else begin
			regInit <= regKey;
		end
	end 
end

assign outData = regData;
assign outMi 	= regInit & 8'hff; 


endmodule
