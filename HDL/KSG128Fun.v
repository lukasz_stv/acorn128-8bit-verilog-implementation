module AcornKSG128Fun (
	input  wire  [7:0] 	inB12,
	input  wire  [7:0]   inB154,
	input  wire  [7:0]   inB235,
	input  wire  [7:0]   inB61,
	input  wire  [7:0]   inB193,
	input  wire  [7:0]   inB230,
	input  wire  [7:0]   inB111,
	input  wire  [7:0]   inB66,
	output wire  [7:0] 	outData
);
// Generated keystream byte
assign outData = inB12 ^ inB154 ^ ( (inB235 & inB61) ^ (inB235&inB193) ^ (inB61 & inB193) ) ^ ((inB230 & inB111)^((~(inB230))&inB66));

endmodule
