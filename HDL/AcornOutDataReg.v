module AcornOutDataReg (
		
		input  wire  			inClk,
		input  wire [7:0]		inData,
		input  wire [7:0]		inKSi,
		input  wire 			inDataWr,
		output wire [7:0]		outData
);

reg [7:0]	regData = 1'b0;

always @ (posedge(inClk)) begin
	if( inDataWr == 1'b1 )begin
		regData <= inData ^ inKSi;
	end
end

assign outData = regData;

endmodule
